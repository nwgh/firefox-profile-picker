#!/usr/bin/env python

import argparse
import functools
import json
import os
import sys

CONF_FILE = os.path.join(os.environ['HOME'], '.fpp.json')
DEFAULT_PROFILEDIR = os.path.join(os.environ['HOME'], '.mozprofiles')


def run_mach(config, profile, rr):
    args = ['--log-no-times', 'run']
    if rr:
        args.append('--debugger=rr')

    if not profile:
        config['use_mach'] = True
    else:
        config['use_mach'] = False
        config['last_profile'] = profile
        args.extend(['-profile', profile])

    try:
        with file(CONF_FILE, 'w') as f:
            json.dump(config, f)
    except Exception:
        sys.stdout.write('WARNING: could not save config\n')

    sys.stdout.write("os.execv('./mach', %s)\n" % (args,))
    #os.execv('./mach', args)


def create_profile(config, profiles, rr):
    done = False
    while not done:
        new_profile = raw_input('Profile Name:\n> ').strip()
        if new_profile and new_profile not in profiles:
            os.makedirs(os.path.join(config['profiledir'], new_profile))
            done = True
        else:
            sys.stdout.write('Invalid profile name\n')
    run_mach(config, new_profile, rr)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--rr', action='store_true')
    args = parser.parse_args()

    try:
        with file(CONF_FILE, 'r') as f:
            config = json.load(f)
    except Exception:
        config = {'use_mach': True,
                  'last_profile': None,
                  'profiledir': DEFAULT_PROFILEDIR}

    try:
        profiles = sorted(os.listdir(config['profiledir']))
    except Exception:
        profiles = []

    choices = {1: functools.partial(run_mach, config, None, args.rr),
               2: functools.partial(create_profile, config, profiles, args.rr)}

    sys.stdout.write('Choose a profile:\n')
    sys.stdout.write(' 1. <Temporary Profile>%s\n' % (' (last used)' if config['use_mach'] else '',))
    sys.stdout.write(' 2. <New Profile>\n')

    for i, p in enumerate(profiles, 3):
        tag = ''
        if p == config['last_profile'] and not config['use_mach']:
            tag = ' (last used)'
        sys.stdout.write(' %d. %s%s\n' % (i, p, tag))
        choices[i] = functools.partial(run_mach, config, p, args.rr)

    done = False
    while not done:
        try:
            choice = int(raw_input('> '))
        except ValueError:
            choice = 0
        if choice not in choices:
            sys.stdout.write('Invalid choice\n')
        else:
            done = True

    choices[choice]()

if __name__ == '__main__':
    main()

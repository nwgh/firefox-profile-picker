const fs = require('fs');
const path = require('path');
const child_process = require('child_process');
const app = require('electron').remote.app;
const ipcRenderer = require('electron').ipcRenderer;

const conf_file = path.join(process.env.HOME, '.fpp.json');
const default_profiledir = path.join(process.env.HOME, '.mozprofiles');
let config = {"last_profile": null,
              "use_mach": true,
              "profiledir": default_profiledir};
let use_rr = false;

ipcRenderer.on('enable-rr', () => {
    use_rr = true;
});

console.log('about to readFile');

fs.readFile(conf_file, (err, data) => {
    console.log('readFile completed');
    var profiles = [];

    if (!err) {
        try {
            config = JSON.parse(data);
        } catch (e) {
            // Couldn't parse the config, write a new one
            fs.writeFileSync(conf_file, config);
            console.log(e);
        }
    } else {
        // Couldn't read the config, write a new one
        fs.writeFileSync(conf_file, config);
        console.log(err);
    }
    console.log(config);

    var disable_userprofiles = false;
    if (config.use_mach) {
        disable_userprofiles = true;
        document.getElementById("profiletype-user").checked = false;
        document.getElementById("profiletype-system").checked = true;
    } else {
        document.getElementById("profiletype-user").checked = true;
        document.getElementById("profiletype-system").checked = false;
    }
    document.getElementById("profiletype-user").addEventListener("click", onRadioChecked);
    document.getElementById("profiletype-system").addEventListener("click", onRadioChecked);

    console.log('about to readdir');
    fs.readdir(config.profiledir, (err, profiles) => {
        console.log('reddir completed');
        if (err) {
            console.log(err);
            profiles = [];
        }

        var profileDiv = document.getElementById("user-profiles");

        for (var i = 0; i < profiles.length; i++) {
            var radio = document.createElement("input");
            radio.setAttribute("type", "radio");
            radio.setAttribute("name", "profile");
            radio.setAttribute("value", profiles[i]);
            radio.setAttribute("id", "profile-" + profiles[i]);
            if (disable_userprofiles) {
                radio.setAttribute("disabled", "disabled");
            }
            if (profiles[i] == config.last_profile) {
                radio.checked = true;
            }
            radio.addEventListener("click", onRadioChecked);

            var label = document.createElement("label");
            label.setAttribute("name", "profile-label");
            label.setAttribute("for", "profile-" + profiles[i]);
            if (disable_userprofiles) {
                label.setAttribute("disabled", "disabled");
            }

            var labelText = document.createTextNode(profiles[i]);
            label.appendChild(labelText);

            var div = document.createElement("div");
            div.appendChild(radio);
            div.appendChild(label);

            profileDiv.appendChild(div);
        }
    });
});

function disableProfiles() {
    console.log("disabling user profiles");
    var radios = document.getElementsByName("profile");
    var labels = document.getElementsByName("profile-label");
    if (radios.length != labels.length) {
        console.error("Too many labels for the number of radios?!");
    }

    for (var i = 0; i < radios.length; i++) {
        radios[i].setAttribute("disabled", "disabled");
        labels[i].setAttribute("disabled", "disabled");
    }
}

function enableProfiles() {
    console.log("enabling user profiles");
    var radios = document.getElementsByName("profile");
    var labels = document.getElementsByName("profile-label");
    console.log("number of radios: " + radios.length);
    if (radios.length != labels.length) {
        console.error("Too many labels for the number of radios?!");
    }

    for (var i = 0; i < radios.length; i++) {
        console.log("enabling profile " + i);
        radios[i].removeAttribute("disabled");
        labels[i].removeAttribute("disabled");
    }
}

function onRadioChecked(event) {
    if (event.target.name == "profile") {
        config.last_profile = event.target.value;
    } else if (event.target.name == "profiletype") {
        if (event.target.id == "profiletype-system") {
            disableProfiles();
            config.use_mach = true;
        } else {
            enableProfiles();
            config.use_mach = false;
        }
    }
    fs.writeFileSync(conf_file, JSON.stringify(config));
}

function startFirefox(event) {
    console.log("starting firefox");
    var args = ["--log-no-times", "run"];

    if (use_rr) {
        args.push("--debugger=rr");
    }

    if (!config.use_mach) {
        if (!config.last_profile) {
            console.log("missing profile?!");
            return;
        }
        var profile = path.join(config.profiledir, config.last_profile);
        args.push("-profile");
        args.push(profile);
    }

    document.getElementById("profile-picker").style.display = "none";
    document.getElementById("firefox-running").style.display = "block";

    console.log(args);

    var exit_code = 0;
    try {
        child_process.execFileSync("./mach", args, {"stdio": "inherit"});
    } catch (e) {
        console.log(e);
        exit_code = e.status;
    }

    app.exit(exit_code);
}

document.getElementById("start-firefox").addEventListener("click", startFirefox);
